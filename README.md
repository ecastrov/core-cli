# CORE-CLI

## COMMANDS

- new <name>
- generate <element> <name>

## Installation

Install locally for development purpose

```sh
$ npm install -g
$ core-cli help
```
