module.exports = {
  verbose: true,
  testEnvironment: 'node',
  testPathIgnorePatterns: ['<rootDir>/(lib|build|docs|node_modules)/'],
  coveragePathIgnorePatterns: ['<rootDir>/node_modules/']
};
