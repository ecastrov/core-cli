#!/usr/bin/env node

'use strict';

const chalk = require('chalk');
const files = require('./libs/files');

if (files.directoryExists('.git')) {
  //IN THE FUTURE POSSIBLE HANDLED THIS CASE FOR DEVELOPMENT PURPOSE
  console.log(chalk.red('Already a git repository!'));
  process.exit();
}

require('./program');
