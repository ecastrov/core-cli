'use strict';

const camelCase = require('camelcase');

const controller = name => `
  'use strict';
  
  const ${camelCase(name)}Lib = require('../libs/${camelCase(name)}Lib');
  
  module.exports = {};`;

const lib = name => `'use strict';
  
  const ${camelCase(name)}Repo = require('../repositories/${camelCase(
  name
)}Repository');
  
  module.exports = {};`;

const repository = () =>
  `'use strict';
  
  const knex = require('../db');
  
  module.exports = {};`;

module.exports = {
  controller,
  lib,
  repository
};
