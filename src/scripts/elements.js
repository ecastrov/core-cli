'use strict';

const clear = require('clear');
const CLI = require('clui');
const Spinner = CLI.Spinner;
const builder = require('../libs/builder');
const elementTypes = require('../enums/elementTypes');

const addElement = (element, name) => {
  clear();
  if (!name || typeof name !== 'string') {
    console.error('You must define what element name you need create');
    process.exit(1);
  }
  if (!element || typeof element !== 'string') {
    console.error('You must define what kind element you need create');
    process.exit(1);
  }
  if (!Object.keys(elementTypes).includes(element.toUpperCase())) {
    console.error(
      `Only valid elements ${Object.keys(elementTypes).toString()}`
    );
  }
  let status = new Spinner('Adding elements...');
  status.start();
  switch (element.toUpperCase()) {
    case elementTypes.REPOSITORY:
      builder.buildRepository(name);
      break;
    case elementTypes.LIB:
      builder.buildLib(name);
      break;
    case elementTypes.CONTROLLER:
      builder.buildController(name);
      break;
  }
  status.stop();
};

module.exports = { addElement };
