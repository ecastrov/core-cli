'use strict';

const clear = require('clear');
const CLI = require('clui');
const Spinner = CLI.Spinner;
const fs = require('fs');
const shell = require('shelljs');

const create = async name => {
  clear();
  console.log('Creating a new project');
  let status = new Spinner('Creating project...');
  status.start();
  if (!fs.existsSync(`./${name}`)) {
    fs.mkdirSync(`./${name}`);
  } else {
    console.warn(`${name} directory exists`);
  }
  try {
    shell.cd(name);
    status.message('Creating package.json');
    await shell.exec('npm init -y', { async: true, silent: true });
    status.message('Installing dependencies');
    await shell.exec('npm i body-parser express dotenv newrelic --save', {
      async: true,
      silent: true
    });
    fs.mkdirSync('./src');
    fs.writeFileSync('ceb-project.json', '', {
      encoding: 'utf8'
    });
  } catch (error) {
    shell.echo('Error: npm command failed');
    shell.exit(1);
  } finally {
    status.stop();
  }
};

module.exports = { create };
