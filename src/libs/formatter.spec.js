'use strict';

const { expect } = require('chai');
const formatter = require('./formatter');

describe('Validate formatter functions', () => {
  it('Prettying should return a string when pass a valid string', () => {
    const content = formatter.prettying('');
    expect(content).to.be.a('string');
  });

  it('Prettying should throw a type error if pass a value different a string', () => {
    expect(formatter.prettying.bind({})).to.throw(TypeError);
  });

  it('camelCasing should return a string when pass a valid string', () => {
    const content = formatter.camelCasing('some-text');
    expect(content).to.be.a('string');
  });

  it('camelCasing should throw a type error if pass a value different a string', () => {
    expect(formatter.camelCasing.bind({})).to.throw(TypeError);
  });
});
