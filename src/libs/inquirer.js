const inquirer = require('inquirer');

const askElementProperties = () => {
  const questions = [
    {
      type: 'input',
      name: 'nameElement',
      message: 'Describe the name for the element to build'
    }
  ];
  return inquirer.prompt(questions);
};

module.exports = {
  askTypeProject: () => {
    const questions = [
      {
        type: 'input',
        name: 'nameProject',
        message: 'Describe the name for the project'
      },
      {
        type: 'list',
        name: 'typeProject',
        message: 'Select the type of project you wish create',
        choices: ['core-api', 'internal-api'],
        default: 'internal-api'
      }
    ];
    return inquirer.prompt(questions);
  },
  askElementProperties,
  askGithubCredentials: () => {
    const questions = [
      {
        name: 'username',
        type: 'input',
        message: 'Enter your GitHub username or e-mail address:',
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your username or e-mail address.';
          }
        }
      },
      {
        name: 'password',
        type: 'password',
        message: 'Enter your password:',
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your password.';
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  }
};
