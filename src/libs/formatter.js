'use strict';

const prettier = require('prettier');
const camelCase = require('camelcase');

const defaultOptions = {
  parser: 'babylon',
  singleQuote: true,
  bracketSpacing: true,
  tabWidth: 2
};

const prettying = (content, options = defaultOptions) => {
  if (typeof content !== 'string') {
    throw TypeError('Invalid type content, string expected');
  }
  return prettier.format(content, options);
};

const camelCasing = content => {
  if (typeof content !== 'string') {
    throw TypeError('Invalid type content, string expected');
  }
  return camelCase(content);
};

module.exports = { prettying, camelCasing };
