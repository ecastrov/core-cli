'use strict';

const formatter = require('./formatter');
const coreTemplate = require('../templates/core-api.template');
const files = require('./files');

const DEFAULT_DIRECTORY = '/src';

const build = (name, identifier, directory, template) => {
  name = formatter.camelCasing(name);
  const path = `${directory}/${name}${identifier}.js`;
  files.createDirectory(directory);
  if (files.fileExists(path)) {
    console.warn(`${name} exists`);
    process.exit(1);
  } else {
    const content = formatter.prettying(template);
    files.createFile(path, content);
  }
};

const buildRepository = name => {
  this.directory = `${process.cwd()}${DEFAULT_DIRECTORY}/repositories`;
  this.identifier = 'Repository';
  build(name, this.identifier, this.directory, coreTemplate.repository());
};

const buildLib = name => {
  this.directory = `${process.cwd()}${DEFAULT_DIRECTORY}/libs`;
  this.identifier = 'Lib';
  build(name, this.identifier, this.directory, coreTemplate.lib(name));
};

const buildController = name => {
  this.directory = `${process.cwd()}${DEFAULT_DIRECTORY}/controllers`;
  this.identifier = 'Controller';
  build(name, this.identifier, this.directory, coreTemplate.controller(name));
};

module.exports = {
  buildRepository,
  buildLib,
  buildController
};
