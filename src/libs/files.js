const fs = require('fs');
const path = require('path');

const createDirectory = directory => {
  if (!directoryExists(directory)) {
    fs.mkdirSync(directory);
  }
};

const createFile = (name, content) => {
  fs.writeFileSync(name, content);
};

const directoryExists = filePath => {
  try {
    return fs.statSync(filePath).isDirectory();
  } catch (err) {
    return false;
  }
};

const fileExists = filePath => fs.existsSync(filePath);

const getCurrentDirectoryBase = () => {
  return path.basename(process.cwd());
};

module.exports = {
  getCurrentDirectoryBase,
  directoryExists,
  createDirectory,
  createFile,
  fileExists
};
