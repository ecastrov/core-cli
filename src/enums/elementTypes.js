'use strict';

module.exports = Object.freeze({
  REPOSITORY: 'REPOSITORY',
  LIB: 'LIB',
  CONTROLLER: 'CONTROLLER'
});
