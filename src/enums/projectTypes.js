'use strict';

module.exports = Object.freeze({
  INTERNAL_API: { name: 'internal-api', type: 'api', useByDefaultKnex: false },
  CORE_API: { name: 'core-api', type: 'api', useByDefaultKnex: true }
});
