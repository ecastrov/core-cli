'use strict';

const program = require('commander');
const { version, description } = require('../package.json');
const projects = require('./scripts/projects');
const elements = require('./scripts/elements');
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');

program.version(version).description(description);

/**Here import all scripts you have to add */
program.command('*').action(() => {
  clear();
  console.log(
    chalk.yellow(figlet.textSync('CEB-CORE-CLI', { horizontalLayout: 'full' }))
  );
});

program
  .command('new <name>')
  .alias('n')
  .description('Create a new starter project')
  .action(projects.create);

program
  .command('generate <element> <name>')
  .alias('g')
  .description('generate a element')
  .action(elements.addElement);

program.parse(process.argv);

module.exports = program;
